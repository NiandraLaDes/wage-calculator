# Project Title

Wage Calculator

# Description

A Wage calculator where you can set hourly wage, hours etc.
Also you can upload a list of numbers and it will calculate the subttal, tax and total

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details