import React from 'react';
import PropTypes from 'prop-types';

const TotalsItem = ({value, children}) => {
    return (
        <div className="totals-item">
            <label>{children}</label>
            <div className="totals-value">{value}</div>
        </div>
    )
}

TotalsItem.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    children: PropTypes.node
}

export default TotalsItem;