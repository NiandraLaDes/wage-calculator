import React, {Component} from 'react';
import PropTypes from 'prop-types';

const Nav = ({onTabChange, activeTab}) => {
    return (
        <nav className="main-nav">
            <ul>
                <li className={`${activeTab === 0 && 'selected'}`}>
                    <NavLink index={0} onClick={onTabChange}>Hours</NavLink>
                </li>
                <li className={`${activeTab === 1 && 'selected'}`}>
                    <NavLink index={1} onClick={onTabChange}>Total</NavLink>
                </li>
            </ul>
        </nav>
    )
}

class NavLink extends Component {
    handleClick = () => {
        this.props.onClick(this.props.index)
    }

    render() {
        return (
            <a onClick={this.handleClick}>{this.props.children}</a>
        )
    }
}

Nav.propTypes = {
    activeTab: PropTypes.number.isRequired,
    onTabChange: PropTypes.func.isRequired,
}

export default Nav;