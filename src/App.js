import React, {Component} from 'react';
import {calculateHourlyTotal, calculateHours, calculateTime, calculateTax, calculateTotal, formatCurrency} from './utils';
import Input from './Input';
import Textarea from './Textarea';
import Nav from './Nav';
import TotalsItem from './TotalsItem';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTab: 0,
            hourlyWage: 50,
            hourlyTotal: '',
            hours: '',
            amountList: '',
            subtotal: '',
            tax: 21
        }

        this.handleChangeHourlyWage = this.handleChangeHourlyWage.bind(this);
        this.handleChangeHourlyTotal = this.handleChangeHourlyTotal.bind(this);
        this.handleChangeHours = this.handleChangeHours.bind(this);
        this.handleChangeAmountList = this.handleChangeAmountList.bind(this);
    }

    handleTabChange = (index) => {
        this.setState({
            activeTab: index
        })
    }

    handleChangeHourlyWage = (event) => {
        let {hours, hourlyTotal} = this.state;
        let hourlyWage = event.target.value;
        hourlyWage = hourlyWage.replace(/[a-zA-Z]/g, '');

        //if hours is not empty, calculate hourly total with new hourly wage
        if (hours !== '') {
            hourlyTotal = calculateHourlyTotal(hours, hourlyWage);
        }

        this.setState((state, props) => {
            return {hours, hourlyWage, hourlyTotal}
        });
    }

    handleChangeHours = (event) => {
        let {hourlyWage} = this.state;
        let hours = event.target.value;
        hours = hours.replace(/[a-zA-Z]/g, '');
        let hourlyTotal = calculateHourlyTotal(hours, hourlyWage);

        this.setState((state, props) => {
            return {hours, hourlyTotal}
        });
    }

    handleChangeHourlyTotal = (event) => {
        let {hourlyWage} = this.state;
        let hourlyTotal = event.target.value;
        let hours = calculateHours(hourlyTotal, hourlyWage);

        this.setState((state, props) => {
            return {hours, hourlyTotal}
        });
    }

    handleChangeAmountList = (event) => {
        let amountList = event.target.value;

        //split values by new space and format them as decimals and filter out isNaN
        let amounts = amountList.split(/\n/).map((amount) => {
            amount = parseFloat(amount.replace(',','.').replace(/[^0-9-.]/g, ''));
            return amount;
        }).filter(value => !Number.isNaN(value));

        //add up all the values
        let subtotal = amounts.reduce((current, total) => {
            return current + total
        }, 0);

        this.setState({amountList, subtotal});
    }

    render() {
        const {hourlyWage, hours, hourlyTotal, activeTab, subtotal, tax, amountList} = this.state;
        return (
            <div className="App">
                <Nav activeTab={activeTab} onTabChange={this.handleTabChange} />
                <main>
                    {activeTab === 0 ?
                        <section>
                            <Input value={hourlyWage} onChange={this.handleChangeHourlyWage}>Hourly Wage</Input>
                            <Input value={hours} onChange={this.handleChangeHours}>Hours</Input>
                            <Input value={hourlyTotal} onChange={this.handleChangeHourlyTotal}>Hourly Total</Input>
                            {hours !== '' &&
                                <section className="totals">
                                    <TotalsItem value={formatCurrency(hourlyWage)}>Wage</TotalsItem>
                                    <TotalsItem value={calculateTime(hours)}>Hours</TotalsItem>
                                    <TotalsItem value={formatCurrency(hourlyTotal)}>Total</TotalsItem>
                                </section>
                            }
                        </section>
                        :
                        <section>
                            <Textarea onChange={this.handleChangeAmountList}>{amountList}</Textarea>
                            {subtotal !== '' &&
                                <section className="totals">
                                    <TotalsItem value={formatCurrency(subtotal)}>Subtotal</TotalsItem>
                                    <TotalsItem value={calculateTax(subtotal, tax)}>Tax ({tax}%)</TotalsItem>
                                    <TotalsItem value={calculateTotal(subtotal, tax)}>Total</TotalsItem>
                                </section>
                            }
                        </section>
                    }
                </main>
            </div>
        );
    }
}

export default App;


