import React from 'react';
import PropTypes from 'prop-types';

const Input = ({value, children, onChange}) => {
    const name = children.replace(/^([A-Z])|\s(\w)/g, function(match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();
    });

    return (
        <div className="form-input">
            <input type="text" autoComplete="off" placeholder={children} value={value} name={name} onChange={onChange}/>
        </div>
    )
}

Input.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    children: PropTypes.node.isRequired,
    onChange: PropTypes.func.isRequired
}

export default Input;