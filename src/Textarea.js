import React from 'react';
import PropTypes from 'prop-types';

const Textarea = ({children, onChange}) => {
    return (
        <div className="form-input">
            <textarea value={children} autoComplete="off" onChange={onChange} />
        </div>
    )
}

Textarea.propTypes = {
    children: PropTypes.node.isRequired,
    onChange: PropTypes.func.isRequired
}

export default Textarea;