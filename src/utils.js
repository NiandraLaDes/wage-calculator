export const calculateHours = (hourlyTotal, hourlyWage) => {
    let hours = '';

    //calculate hours if hourly wage and hourly total are both filled
    if(hourlyTotal !== '' && hourlyWage !== '') {
        let minutes = Number((hourlyTotal / wagePerMinute(hourlyWage)).toFixed(2));
        hours = Math.trunc((minutes / 60));
        let totalMinutes = (minutes - (hours * 60)) / 100;
        hours = Number((hours + totalMinutes).toFixed(2));
    }
    return hours;
}

export const calculateHourlyTotal = (hours, hourlyWage) => {
    let hourlyTotal = '';

    //calculate minutes and hourlytotal when hours and hourly wage are both filled
    if(hours !== ''  && hourlyWage !== '')
    {
        let totalHours = Math.trunc(hours);
        let totalMinutes = Number((hours-totalHours).toFixed(2)*100);
        totalMinutes += Number((totalHours * 60).toFixed(2));
        hourlyTotal = Number((totalMinutes * wagePerMinute(hourlyWage)).toFixed(2));
    }
    return hourlyTotal;
}

export const formatCurrency = (n) => {
    let currency = '€ ';
    return currency + Number(n).toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

export const calculateTime = (hours) => {
    const totalHours = Math.trunc(hours);
    let totalMinutes = Number(((hours-totalHours)*100).toFixed(2));
    return totalHours + ' hours and ' + totalMinutes + ' minutes';
}

export const calculateTax = (subtotal, tax) => {
    let n = parseFloat((subtotal * (tax / 100)).toFixed(2));
    return formatCurrency(n);
}

export const calculateTotal = (subtotal, tax) => {
    let n = parseFloat((subtotal * ((tax / 100) + 1)).toFixed(2));
    return formatCurrency(n);
}

const wagePerMinute = (hourlyWage) => {
    return Number(hourlyWage / 60);
}